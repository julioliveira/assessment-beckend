<?php

namespace app\model;

/**
 * Interface ModeInterface
 * @package app\model
 */
interface ModeInterface
{
    /**
     * @param object $object
     * @return mixed
     */
    public function insert(object $object);

    /**
     * @param object $object
     * @return mixed
     */
    public function update(object $object);
}
