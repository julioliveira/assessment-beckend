<?php

namespace app\model;

use app\model\entity\Category;
use app\model\entity\Product;
use jboframe\Model\Database;

/**
 * Class ProductDatabase
 * @package app\model
 */
class ProductDatabase extends Database implements ModeInterface
{
    protected $table = "product";

    /**
     * @param Product $product
     * @return bool
     */
    public function insert($product): bool
    {
        try {
            $query = "INSERT INTO {$this->table} SET 
                        nome=:nome,
                        sku=:sku,
                        preco=:preco,
                        quantidade=:quantidade,
                        descricao=:descricao";
            $stmt = $this->db->prepare($query);

            $nome = htmlspecialchars(strip_tags($product->getNome()));
            $sku = htmlspecialchars(strip_tags($product->getSku()));
            $preco = htmlspecialchars(strip_tags($product->getPreco()));
            $quantidade = htmlspecialchars(strip_tags($product->getQuantidade()));
            $descricao = htmlspecialchars(strip_tags($product->getDescricao()));

            $stmt->bindValue(":nome", $nome);
            $stmt->bindValue(":sku", $sku);
            $stmt->bindValue(":preco", $preco);
            $stmt->bindValue(":quantidade", $quantidade);
            $stmt->bindValue(":descricao", $descricao);

            if ($stmt->execute()) {
                $idProduct = $this->db->lastInsertId();

                foreach ($product->getCategorias() as $categoria) {
                    $stmtCat = $this->db->prepare(
                        "INSERT INTO productCategory SET id_product=:idProduct, id_category=:idCategory"
                    );
                    $stmtCat->bindValue(":idProduct", $idProduct);
                    $stmtCat->bindValue(":idCategory", $categoria);
                    $stmtCat->execute();
                }

                return true;
            }

            return false;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @param Product $product
     * @return Product|bool
     */
    public function update($product)
    {
        try {
            $query = "UPDATE {$this->table} SET 
                        nome=:nome,
                        sku=:sku,
                        preco=:preco,
                        quantidade=:quantidade,
                        descricao=:descricao
                      WHERE id=:id";
            $stmt = $this->db->prepare($query);

            $nome = htmlspecialchars(strip_tags($product->getNome()));
            $sku = htmlspecialchars(strip_tags($product->getSku()));
            $preco = htmlspecialchars(strip_tags($product->getPreco()));
            $quantidade = htmlspecialchars(strip_tags($product->getQuantidade()));
            $descricao = htmlspecialchars(strip_tags($product->getDescricao()));

            $stmt->bindValue(":nome", $nome);
            $stmt->bindValue(":sku", $sku);
            $stmt->bindValue(":preco", $preco);
            $stmt->bindValue(":quantidade", $quantidade);
            $stmt->bindValue(":descricao", $descricao);
            $stmt->bindValue(":id", $product->getId());

            if ($stmt->execute()) {
                $idProduct = $product->getId();

                foreach ($product->getCategorias() as $categoria) {
                    $query = "DELETE FROM productCategory WHERE id=:id";
                    $stmtPro = $this->db->prepare($query);
                    $stmtPro->bindParam(":id", $idProduct, \PDO::PARAM_INT);

                    $stmtCat = $this->db->prepare(
                        "INSERT INTO productCategory SET id_product=:idProduct, id_category=:idCategory"
                    );
                    $stmtCat->bindValue(":idProduct", $idProduct);
                    $stmtCat->bindValue(":idCategory", $categoria);
                    $stmtCat->execute();
                }

                return true;
            }

            return false;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @return array
     */
    public function getProductsCategories()
    {
        try {
            $query = "select * from {$this->table}";

            $stmt = $this->db->prepare($query);
            $stmt->execute();

            $produtos = $stmt->fetchAll(
                \PDO::FETCH_CLASS, "app\\model\\entity\\". ucfirst($this->table)
            );
            /**
             * @var $produto Product
             */
            foreach ($produtos as &$produto) {
                $this->setCategoriasByProducts($produto);
            }

            return $produtos;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @param Product $produto
     */
    public function setCategoriasByProducts(Product $produto)
    {
        $stmtPro = $this->db->prepare(
            "select * from category c 
                          join productCategory pC on c.id = pC.id_category 
                          where pC.id_product =:idProduct"
        );
        $stmtPro->bindValue(":idProduct", $produto->getId());
        $stmtPro->execute();
        $categorias = $stmtPro->fetchAll(
            \PDO::FETCH_CLASS, Category::class
        );

        $produto->setCategorias($categorias);
    }
}
