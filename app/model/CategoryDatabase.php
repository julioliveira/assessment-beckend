<?php

namespace app\model;

use app\model\entity\Category;
use jboframe\Model\Database;

/**
 * Class CategoryDatabase
 * @package app\model
 */
class CategoryDatabase extends Database implements ModeInterface
{
    protected $table = "category";

    /**
     * @param object $category
     * @return bool|mixed|string
     */
    public function insert($category)
    {
        try {
            $query = "INSERT INTO {$this->table} SET nome=:nome, code=:code";
            $stmt = $this->db->prepare($query);
            $nome = htmlspecialchars(strip_tags($category->getNome()));
            $code = htmlspecialchars(strip_tags($category->getCode()));

            $stmt->bindValue(":nome", $nome);
            $stmt->bindValue(":code", $code);

            if ($stmt->execute()) {
                return $this->db->lastInsertId();
            }

            return false;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @param Category $category
     * @return Category|bool
     */
    public function update($category)
    {
        try {
            $query = "UPDATE {$this->table} SET nome=:nome, code=:code WHERE id=:id";
            $stmt = $this->db->prepare($query);
            $nome = htmlspecialchars(strip_tags($category->getNome()));
            $code = htmlspecialchars(strip_tags($category->getCode()));

            $stmt->bindValue(":nome", $nome);
            $stmt->bindValue(":code", $code);
            $stmt->bindValue(":id", $category->getId());

            if ($stmt->execute()) {
                return $category;
            }

            return false;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @param string $nome
     * @return mixed
     */
    public function getByName(string $nome)
    {
        try {
            $query = "select * from {$this->table} where nome=:nome limit 1";
            $stmt = $this->db->prepare($query);

            $stmt->bindValue(":nome", $nome);
            $stmt->execute();

            return $stmt->fetch();
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }
}
