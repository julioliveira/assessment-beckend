<?php

namespace app\config;

/**
 * Class Conn
 * @package app\config
 */
class Conn
{
    /**
     * @return \PDO
     */
    public static function getDb()
    {
        try{
            return new \PDO('mysql:host=localhost;dbname=db_assessment', 'admin', '123');
        } catch (\PDOException $exception){
            die("Error! Code: " .$exception->getCode(). "<br/> Message: " .$exception->getMessage());
        }
    }
}
