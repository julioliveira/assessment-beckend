<?php

namespace app\config;

use jboframe\init\Bootstrap;

/**
 * Class Route
 * @package app\config
 */
class Route extends Bootstrap
{

    protected function initRoute()
    {
        $routes['dashboard'] = array('route' => '/assessment-backend/', 'controller' => "indexController", 'action' => 'index');
        $routes['view'] = array('route' => '/assessment-backend/view', 'controller' => "indexController", 'action' => 'view');

        $routes['indexProduct'] = array('route' => '/assessment-backend/product/', 'controller' => "productController", 'action' => 'index');
        $routes['addProduct'] = array('route' => '/assessment-backend/product/add', 'controller' => "productController", 'action' => 'add');
        $routes['editProduct'] = array('route' => '/assessment-backend/product/edit', 'controller' => "productController", 'action' => 'edit');
        $routes['deleteProduct'] = array('route' => '/assessment-backend/product/delete', 'controller' => "productController", 'action' => 'delete');

        $routes['indexCategory'] = array('route' => '/assessment-backend/category/', 'controller' => "categoryController", 'action' => 'index');
        $routes['addCategory'] = array('route' => '/assessment-backend/category/add', 'controller' => "categoryController", 'action' => 'add');
        $routes['editCategory'] = array('route' => '/assessment-backend/category/edit', 'controller' => "categoryController", 'action' => 'edit');
        $routes['deleteCategory'] = array('route' => '/assessment-backend/category/delete', 'controller' => "categoryController", 'action' => 'delete');

        $this->setRoutes($routes);
    }
}