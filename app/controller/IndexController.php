<?php

namespace app\controller;

use app\controller\contracts\ShowInterface;
use jboframe\DI\Container;

/**
 * Class IndexController
 * @package app\controller
 */
class IndexController extends BaseController implements ShowInterface
{
    /**
     * @return mixed|void
     */
    public function index()
    {
        $this->view->title = "Webjump | Backend Test | Dashboard";
        $container = Container::getModel("productDatabase");

        $this->view->produtos = $container->getProductsCategories();

        $this->render("index");

        $this->render("index");
    }

    /**
     * @return mixed|void
     */
    public function view()
    {
        // TODO: Implement view() method.
    }
}
