<?php

namespace app\controller\contracts;

/**
 * Interface ShowInterface
 * @package app\controller\contracts
 */
interface ShowInterface extends ControllerInterface
{
    /**
     * @return mixed
     */
    public function view();
}
