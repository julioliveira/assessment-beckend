<?php

namespace app\controller\contracts;

/**
 * Interface ControllerInterface
 * @package app\controller\contracts
 */
interface ControllerInterface
{
    /**
     * @return mixed
     */
    public function index();
}