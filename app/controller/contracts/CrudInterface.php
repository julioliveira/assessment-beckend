<?php

namespace app\controller\contracts;

/**
 * Interface CrudInterface
 * @package app\controller\contracts
 */
interface CrudInterface extends ControllerInterface
{
    /**
     * @return mixed
     */
    public function add();

    /**
     * @return mixed
     */
    public function edit();

    /**
     * @return mixed
     */
    public function delete();
}
