<?php

namespace app\controller;

use app\model\entity\Product;
use jboframe\DI\Container;

/**
 * Class ProductController
 * @package app\controller
 */
class ProductController extends BaseController
{
    /**
     * @return mixed|void
     */
    public function index()
    {
        $this->view->title = "Produto";
        $container = Container::getModel("productDatabase");

        $this->view->produtos = $container->getProductsCategories();

        $this->render("index");
    }

    /**
     * @return mixed|void
     */
    public function add()
    {
        $this->view->title = "Adicionar Categoria";
        $container = Container::getModel("productDatabase");
        $categoryContainer = Container::getModel("categoryDatabase");

        $this->view->categorias = $categoryContainer->fetchAll();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $product = new Product();
            $product->setSku($_POST['sku']);
            $product->setNome($_POST['nome']);
            $product->setPreco($_POST['preco'] ?? 0);
            $product->setQuantidade($_POST['quantidade'] ?? 0);
            $product->setDescricao($_POST['descricao'] ?? '');
            $product->setCategorias($_POST['categorias']);

            $container->insert($product);
        }

        $this->render("add");
    }

    /**
     * @return mixed|void
     */
    public function edit()
    {
        $this->view->title = "Editar Categoria";
        $container = Container::getModel("productDatabase");
        $categoryContainer = Container::getModel("categoryDatabase");

        $this->view->categorias = $categoryContainer->fetchAll();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $product = new Product();
            $product->setId($_POST['id']);
            $product->setSku($_POST['sku']);
            $product->setNome($_POST['nome']);
            $product->setPreco($_POST['preco'] ?? 0);
            $product->setQuantidade($_POST['quantidade'] ?? 0);
            $product->setDescricao($_POST['descricao'] ?? '');
            $product->setCategorias($_POST['categorias']);

            $container->update($product);
        }

        if (!$_GET['id']) {
            $this->redirect($this->view->baseUrl . '/product/');
        }

        $product = $container->getById($_GET['id']);
        $container->setCategoriasByProducts($product);

        $this->view->product = $product;

        $this->render("edit");
    }

    /**
     * @return mixed|void
     */
    public function delete()
    {
        if (!$_GET['id']) {
            $this->redirect($this->view->baseUrl . '/product/');
        }

        $container = Container::getModel("productDatabase");
        $container->delete((int)$_GET['id']);

        $this->redirect($this->view->baseUrl . '/product/');
    }
}
