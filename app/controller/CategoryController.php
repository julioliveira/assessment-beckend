<?php

namespace app\controller;

use app\controller\contracts\CrudInterface;
use app\model\entity\Category;
use jboframe\DI\Container;

/**
 * Class CategoryController
 * @package app\controller
 */
class CategoryController extends BaseController implements CrudInterface
{
    /**
     * @return mixed|void
     */
    public function index()
    {
        $this->view->title = "Categoria";
        $container = Container::getModel("categoryDatabase");

        $this->view->categorias = $container->fetchAll();

        $this->render("index");
    }

    /**
     * @return mixed|void
     */
    public function add()
    {
        $this->view->title = "Adicionar Categoria";
        $container = Container::getModel("categoryDatabase");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $category = new Category();
            $category->setNome($_POST['nome']);
            $category->setCode( $_POST['code']);

            $container->insert($category);
        }

        $this->render("add");
    }

    /**
     * @return mixed|void
     */
    public function edit()
    {
        $this->view->title = "Editar Categoria";
        $container = Container::getModel("categoryDatabase");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $category = new Category();
            $category->setNome($_POST['nome']);
            $category->setCode( $_POST['code']);
            $category->setId($_POST['id']);

            $container->update($category);
        }

        if (!$_GET['id']) {
            $this->redirect($this->view->baseUrl . '/category/');
        }

        $this->view->category = $container->getById($_GET['id']);

        $this->render("edit");
    }

    /**
     * @return mixed|void
     */
    public function delete()
    {
        if (!$_GET['id']) {
            $this->redirect($this->view->baseUrl . '/category/');
        }

        $container = Container::getModel("categoryDatabase");
        $container->delete($_GET['id']);

        $this->redirect($this->view->baseUrl . '/category/');
    }
}