## Descrição

Assessment-backend - App desenvolvido em PHP

A aplicação foi desenvolvida toda em PHP, criei um mini framework com o nome jboframe (julio barbosa oliveira framework)
somente para pode aplicar o MVC corretamente e demonstrar conhecimento. Com exceção do autoload,
 não foi utilizado nenhum framework ou biblioteca externa.
 
 Projeto foi pensado todo em MVC, com sistema de rotas simples aplicando padrão SOLID e Object Calisthenics

## Requisitos

1. PHP 7.2
2. Composer
3. MySQL


## Instalação/Configuração

1. Execute o arquivo ```database/import/db_Assessment.sql``` dentro de um db com o mesmo nome.

2. Configurar arquivo de conexão ```app/config/Conn.php``` com os parametros de conexão do mysql.

3. Coloque o projeto no raiz do localhost/apache2, para que possa ser acessado pela URL ```http://localhost/assessment-backend/```

Observação: O projeto ta configurado com essa rota, caso deseja muda o URL domínio do projeto deve mudar também as rotas dentro do arquivo ```app/config/Route.php```


## Importando CSV


1. Após configurado o arquivo de conexão só precisa roda o comando ```php -f BuildImportCsv.php ``` dentro da basta database no raiz do projeto.
Irá ter uma resposta dos arquivo inseridos, ex:
```Array
   (
       [0] => Waterless Fragrance Free Hand Sanitizer
       [1] => 55319-501
       [2] => Removal of Spacer from Left Shoulder Joint, Percutaneous Endoscopic Approach
       [3] => 49
       [4] => 9787.28
       [5] => Array
           (
               [0] => Documentary
           )
   
   )
```

## Finalização

Obrigado!
Qualquer dúvida só enviar e-mail para julio.barbosa.15@gmail.com

 
