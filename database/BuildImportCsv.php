<?php

namespace app\database;

require_once(__DIR__ . "./../index.php");

use app\model\entity\Category;
use app\model\entity\Product;
use jboframe\DI\Container;

/**
 * Class BuildImportCsv
 * @package database
 */
class BuildImportCsv
{
    private $path = __DIR__ . '/import/import.csv';

    /**
     * BuildImportCsv constructor.
     */
    public function __construct()
    {
        $this->run();
    }

    /**
     *
     */
    public function run()
    {
        $product = $this->csv_to_array($this->path);
        $this->insert($product);
    }

    /**
     * @param string $filename
     * @return array|bool
     */
    private function csv_to_array($filename = 'import.csv')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $data = [];

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 100000, ';')) !== FALSE) {
                $categorias = explode('|', $row[5]);
                $row[5] = $categorias;
                $data[] = $row;
            }

            fclose($handle);
        }

        return $data;
    }

    /**
     * @param $array
     */
    private function insert($array)
    {
        $productModel = Container::getModel('productDatabase');
        $categoryModel = Container::getModel('categoryDatabase');

        foreach ($array as $product) {
            // return false if there is no data
            if (empty($product)) {
                return;
            }

            $categoriasIds = [];

            foreach ($product[5] as $categoria) {
                $retorno = $categoryModel->getByName($categoria);

                if ($retorno !== false) {
                    $categoriasIds[] = $retorno['id'];

                    continue;
                }

                $newCategory = new Category();
                $newCategory->setNome($categoria);
                $newCategory->setCode($categoria);

                $categoriasIds[] = $categoryModel->insert($newCategory);
            }

            $newProduct = new Product();
            $newProduct->setSku($product[1]);
            $newProduct->setNome($product[0]);
            $newProduct->setPreco($product[4] ?? 0);
            $newProduct->setQuantidade($product[3] ?? 0);
            $newProduct->setDescricao($product[2] ?? '');
            $newProduct->setCategorias($categoriasIds);

            $productModel->insert($newProduct);

            print_r($product);
        }
    }
}

new BuildImportCsv();
