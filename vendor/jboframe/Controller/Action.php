<?php

namespace jboframe\Controller;

/**
 * Class Action
 * @package jboframe\Controller
 */
abstract class Action
{
    /**
     * @var \stdClass
     */
    protected $view;

    /**
     * @var string
     */
    private $action;

    /**
     * Action constructor.
     */
    public function __construct()
    {
        $this->view = new \stdClass;
        $this->view->baseUrl = $this->url();
        $this->view->publicUrl = $this->url() . '/public';
    }

    /**
     * @param string $action
     * @param bool $layout
     */
    protected function render(string $action, $layout = true)
    {
        $this->action = $action;

        if ($layout == true && file_exists("./app/view/layout.phtml")) {
            include_once "./app/view/layout.phtml";
        } else {
            $this->content();
        }
    }

    /**
     *
     */
    protected function content()
    {
        $current = get_class($this);
        $singlesClassName = strtolower(
            (str_replace(
                "Controller",
                "",
                str_replace("app\\controller\\", "", $current))
            )
        );

        include_once "./app/view/$singlesClassName/$this->action.phtml";
    }

    /**
     * @param $url
     * @param int $statusCode
     */
    protected function redirect(string $url, $statusCode = 303)
    {
        header("Location: $url", true, $statusCode);
        die();
    }

    /**
     * @return string
     */
    private function url()
    {
        $protocol = 'http';

        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        $uri = explode('/', $_SERVER['REQUEST_URI']);

        return $protocol . "://" . $_SERVER['HTTP_HOST'] . "/" . $uri[1];
    }
}