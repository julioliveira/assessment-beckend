<?php

namespace jboframe\DI;

use app\config\Conn;

/**
 * Class Container
 * @package jboframe\DI
 */
class Container
{
    /**
     * @param string $model
     * @return mixed
     */
    public static function getModel(string $model)
    {
        $class = "\\app\\model\\" . ucfirst($model);

        return new $class(Conn::getDb());
    }
}