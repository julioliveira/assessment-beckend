<?php

namespace jboframe\Model;

/**
 * Class Database
 * @package jboframe\Model
 */
abstract class Database
{
    /**
     * @var \PDO
     */
    protected $db;

    /**
     * @var string
     */
    protected $table;

    /**
     * Database constructor.
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        try {
            $query = "select * from {$this->table}";

            $stmt = $this->db->prepare($query);
            $stmt->execute();

            return $stmt->fetchAll(
                \PDO::FETCH_CLASS, "app\\model\\entity\\". ucfirst($this->table)
            );
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        try {
            $stmt = $this->db->query("select * from {$this->table} where id=$id")
                ->fetchObject("app\\model\\entity\\". ucfirst($this->table));

            return $stmt;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        try {
            $query = "DELETE FROM {$this->table} WHERE id=:id";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                return true;
            }

            return false;
        } catch (\PDOException $exception) {
            die("Erro: " . $exception->getMessage());
        }
    }
}
