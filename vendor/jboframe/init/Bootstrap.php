<?php

namespace jboframe\init;

/**
 * Class Bootstrap
 * @package jboframe\init
 */
abstract class Bootstrap
{
    /**
     * @var array
     */
    private $routes;

    /**
     * Bootstrap constructor.
     */
    public function __construct()
    {
        $this->initRoute();
        $this->runApp($this->getUrl());
    }

    /**
     * @return mixed
     */
    abstract protected function initRoute();

    /**
     * @param $url
     */
    protected function runApp($url)
    {
        array_walk($this->routes, function ($route) use ($url) {

            if ($url == $route['route']) {
                $class = "app\\controller\\" . ucfirst($route['controller']);
                $controller = new $class;
                $action = $route['action'];
                $controller->$action();
            }
        });
    }

    /**
     * @param array $routes
     */
    protected function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @return mixed
     */
    protected function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }
}